﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonRotateEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool rotate = false;
    public float Angle = 2.0f;

    public void OnPointerEnter(PointerEventData eventData)
    {
        rotate = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rotate = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(rotate)
            transform.Rotate(Vector3.back, Angle);
	}
}
