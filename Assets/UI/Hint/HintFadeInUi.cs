﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class HintFadeInUi : MonoBehaviour
{
    public GameObject BindObject;
    private RectTransform rectTransform;

    public float Duration = 1f;
    public Vector3 PositionOffset = Vector3.zero;

    private Image image;

    public bool PlayOnStart = false;

    public bool EnableMove = false;
    public Vector3 MoveOffset = Vector3.zero;

    public float HideDelay = 5f;

    // Use this for initialization
    void Start()
    {
        image = GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0);

        rectTransform = GetComponent<RectTransform>();

        if (EnableMove)
        {
            rectTransform.position += MoveOffset;
        }
        else
        {
            if (BindObject)
                UpdatePosition();
        }

        if (PlayOnStart)
            Show();

        if (HideDelay > 0)
            Invoke("Hide", HideDelay);
    }

    public void Show()
    {
        if(EnableMove)
        {
            rectTransform.DOMove(rectTransform.position - MoveOffset, Duration);
        }
        image.DOFade(1, Duration);
    }

    public void Hide()
    {
        if(EnableMove)
        {
            rectTransform.DOMove(rectTransform.position + MoveOffset, Duration);
        }
        image.DOFade(0, Duration);
    }

    void UpdatePosition()
    {
        var p = Camera.main.WorldToScreenPoint(BindObject.transform.position);
        rectTransform.position = p+PositionOffset;
    }

    // Update is called once per frame
    void Update()
    {
        if (BindObject)
            UpdatePosition();
    }
}
