﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class HintFadeInScene : MonoBehaviour {

    public Sprite Sprite;

    private Transform childTransform;

    public float Duration = 1f;
    public Vector3 Offset = Vector3.up;
    public Vector3 Position;

    private SpriteRenderer renderer;

    public bool EnterCollisionShow = false;

    public bool CanCollisionPlayer=true;
    public bool CanCollisionDrone=false;

    private bool showed = false;

    // Use this for initialization
    void Start () {
        var g=new GameObject();
        g.transform.parent = transform;
        g.transform.localPosition = Position;
        renderer = g.AddComponent<SpriteRenderer>();
        
            renderer.sprite= Sprite;
        renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0);

        childTransform = g.transform;

        //Show();
    }

    public void Show()
    {
        if (GameManager.Instance.CurrentViewMode == "Z")
        {
            Debug.Log("Rotate");
            childTransform.transform.Rotate(0, -90, 0);
        }
        else if (GameManager.Instance.CurrentViewMode == "X")
        {
            Debug.Log("Rotate");
            childTransform.transform.Rotate(0, 0, 0);
        }
        else
        {
            childTransform.transform.Rotate(0, -180, 0);
        }
        childTransform.position += Offset;
        childTransform.DOMove(childTransform.position-Offset, Duration);
        renderer.DOFade(1, Duration);

        showed = true;
    }

    public void Hide()
    {
        renderer.DOFade(0, Duration);
        showed = false;
    }
	
	// Update is called once per frame
	void Update () {
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (showed) return;
        if (!EnterCollisionShow) return;

        if(CanCollisionPlayer)
            if(collision.collider.GetComponent<PlayerControl>() != null)
                Show();

        if(CanCollisionDrone)
            if (collision.collider.GetComponent<DroneControl>() != null)
                Show();

    }
}
