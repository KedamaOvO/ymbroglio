﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyRay : MonoBehaviour {
    public bool DisplayRays=false;

    public GameObject LineRendererPrefab;

    public int RayCount = 150;
    public int MaxRefacleCount = 5;

    public float MaxRayDistance = 10;
    public float Angle=60;
    private List<LineRenderer> lineRenderers=new List<LineRenderer>();

    public bool BotInRange = false;

	// Use this for initialization
	void Start () {
		for(int i=0;i< RayCount; i++)
        {
            var line_object = GameObject.Instantiate(LineRendererPrefab, transform);
            line_object.name += "-"+i.ToString();
            line_object.transform.parent = transform;
            lineRenderers.Add(line_object.GetComponent<LineRenderer>());
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            DisplayRays = !DisplayRays;

        }
		if (Input.GetAxis("RightX") == 0 && Input.GetAxis("RightY") == 0 && !GameManager.Instance.RayMouseMode)
		{
			DisplayRays = false;
		}
		else if(!GameManager.Instance.RayMouseMode)
		{
			DisplayRays = true;
		}
		

		if (DisplayRays == false)
		{
			foreach (var renderer in lineRenderers)
				renderer.positionCount = 0;
		}
		if (!DisplayRays) return;
		Vector3 dir;
		if (GameManager.Instance.RayMouseMode)
		{
			Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if(Physics.Raycast(mouseRay, out hitInfo))
				if (hitInfo.collider.gameObject.GetComponent<PlayerControl>() != null) return;

			var pp = Camera.main.WorldToScreenPoint(transform.position);
			pp.z = 0;
			var mp = Input.mousePosition;
			dir = (mp - pp).normalized;
		}
		else //joystick mode
		{
			float rightx = Input.GetAxis("RightX");
			float righty = Input.GetAxis("RightY");
			dir = new Vector3(rightx, righty, 0).normalized;
		}

		
        BotInRange=TransmitEnergyLines(dir);
		GameManager.Instance.IsEnergyOn = BotInRange;
    }

    //生成射线
    private bool TransmitEnergyLines(Vector3 direction)
    {
        Vector3 dir = direction;
        float half_angle = Angle * 0.5f;
        float ang_step = Angle * Mathf.Deg2Rad / RayCount;

        bool hit_bot = false;

        for (int i = 0; i < RayCount / 2; i++)
        {
            hit_bot = TransmitEnergyLine(lineRenderers[i],dir)||hit_bot;
            dir = Rotate(dir, ang_step);
        }

        dir = Rotate(direction,-ang_step);

        for (int i = RayCount / 2; i < RayCount; i++)
        {
            hit_bot = TransmitEnergyLine(lineRenderers[i], dir)||hit_bot;
            dir = Rotate(dir, -ang_step);
        }

        return hit_bot;
    }

    //处理每根射线的反射
    private bool TransmitEnergyLine(LineRenderer renderer,Vector3 dir)
    {
        int reflect_count = 0;
        float ray_length = 0;
        bool hit_bot = false;

        List<Vector3> points = new List<Vector3>(MaxRefacleCount+1);
        Vector3 last_point = transform.position;
        points.Add(last_point);

        while (reflect_count< MaxRefacleCount&&ray_length<MaxRayDistance)
        {
            RaycastHit hit;
            if(Physics.Raycast(last_point, dir,out hit,MaxRayDistance, LayerMask.GetMask("Reflectable","Player","Default")))
            {
				if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Reflectable"))
				{
					dir = Vector3.Reflect(dir, hit.normal);
					//ray_length = hit.distance;

					last_point = hit.point;
					reflect_count++;
					points.Add(last_point);
				}
				else if (hit.collider.gameObject.GetComponent<DroneControl>() != null)
				{
					last_point = hit.point;
					points.Add(last_point);
					hit_bot = true;
					break;
				}
				else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Default"))
				{
					last_point = hit.point;
					points.Add(last_point);
					break;
				}
				else
				{
					points.Add(last_point + dir * MaxRayDistance);
					break;
				}
            }
            else
            {
                points.Add(last_point + dir*MaxRayDistance);
                break;
            }
        }

        renderer.positionCount = points.Count;
        renderer.SetPositions(points.ToArray());

        return hit_bot;
    }

    public Vector2 Rotate(Vector3 dir, float ang)
    {
        Vector2 r;
        r.x = Mathf.Cos(ang) * dir.x - Mathf.Sin(ang) * dir.y;
        r.y = Mathf.Cos(ang) * dir.y + Mathf.Sin(ang) * dir.x;
        return r;
    }
}
