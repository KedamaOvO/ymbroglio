﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonExControl : MonoBehaviour {

    public bool TouchToTrigger = true;
    public List<TriggerBase> InteractiveObjList;
    // Use this for initialization
    public bool UntouchToOff;
    public bool IsOn = false;

	public bool PressKeyToTrigger = false;

	public KeyCode PressKeyCode=KeyCode.P;

	public bool InScope;

	public bool IsShowTips = false;

	public GameObject TipsUI;

	public bool Is2DTips=true;

	public Vector3 TipsOffset=Vector3.zero;

	public bool IsBoxButton;

	public new AudioClip audio;
	void Start () {
		
	}
	


	// Update is called once per frame
	void Update () {
		

		if (PressKeyToTrigger && InScope)
		{
			if (Input.GetKeyDown(PressKeyCode)||Input.GetButtonDown("Fire2"))
			{
				if (!IsOn)
				{

					DoTrigger();
				}
				else 
				{
					DoTriggerOff();
				}		
			}
		}
	}

	private void OnGUI()
	{
		if (IsShowTips && InScope && Is2DTips)
		{
			Vector2 pos = GameManager.Instance.MainCamera.GetComponent<Camera>().WorldToScreenPoint(this.transform.position + TipsOffset);
			TipsUI.GetComponent<Image>().transform.position = pos;
		}
	}


	private void OnTriggerEnter(Collider other)
    {
		GameObject collObj = other.gameObject;
		if (IsBoxButton)
		{
			if (collObj.GetComponent<BoxControl>() != null)
			{
				DoTrigger();
			}

		}
		if (collObj.GetComponent<PlayerControl>() == null && collObj.GetComponent<DroneControl>() == null) return;


		InScope = true;
		if (IsShowTips) ShowTips();

		if (IsBoxButton) return; 

		if (TouchToTrigger && !IsOn)
        {
            
            DoTrigger();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
		GameObject collObj = collision.gameObject;
		if (IsBoxButton)
		{
			if (collObj.GetComponent<BoxControl>() != null)
			{
				DoTrigger();
			}


		}
		if (collObj.GetComponent<PlayerControl>() == null && collObj.GetComponent<DroneControl>() == null) return;
		InScope = true;
		if (IsShowTips) ShowTips();

		if (IsBoxButton) return;
		if (TouchToTrigger && !IsOn)
        {
            DoTrigger();
        }

		
    }

    private void OnTriggerExit(Collider other)
    {
		InScope = false;
        GameObject collobj = other.gameObject;
		if (UntouchToOff && IsOn && TouchToTrigger && !IsBoxButton )
        { 
            DoTriggerOff();
        }
        else if (UntouchToOff && IsOn && TouchToTrigger && IsBoxButton && collobj.GetComponent<BoxControl>()!=null)
        {
            DoTriggerOff();
        }


        HideTips();
    }

    private void OnCollisionExit(Collision collision)
    {
		InScope = false;
		if (UntouchToOff && IsOn && TouchToTrigger)
        {  
            DoTriggerOff();
        }
		HideTips();
	}

    public void DoTrigger()
    {
		if (audio != null)
			GameManager.Instance.GameAudioSource.PlayOneShot(audio);
		//print("touched");
		foreach (TriggerBase obj in InteractiveObjList)
        {
			IsOn = true;
            obj.TriggerOn();
        }
    }

    public void DoTriggerOff()
    {
		if (audio != null)
			GameManager.Instance.GameAudioSource.PlayOneShot(audio);
		foreach (TriggerBase obj in InteractiveObjList)
        {
			// print("triggerOff");
			IsOn = false;
			obj.TriggerOff();
        }
    }


	private void ShowTips()
	{
		if (TipsUI != null)
		{
			if (Is2DTips)
			{
				Vector2 pos=GameManager.Instance.MainCamera.GetComponent<Camera>().WorldToScreenPoint(this.transform.position + TipsOffset);
				TipsUI.GetComponent<Image>().transform.position = pos;
				TipsUI.SetActive(true);
			}
			else
			{
				TipsUI.transform.position = this.transform.position + TipsOffset;
				TipsUI.SetActive(true);
			}

		}
	}


	private void HideTips()
	{
		if (TipsUI != null)
		{
			TipsUI.SetActive(false);
		}
	}
}
