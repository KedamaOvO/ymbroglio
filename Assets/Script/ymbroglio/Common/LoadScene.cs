﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;

public class LoadScene : PlayableAsset, IPlayableBehaviour
{
    GameObject Loading;
    public string SceneName;

    public void OnGraphStart(Playable playable)
    {
    }

    public void OnBehaviourPlay(Playable playable, FrameData info)
    {
        Loading = GameObject.FindObjectOfType<Canvas>().gameObject.transform.GetChild(0).gameObject;
        Loading.SetActive(true);
        SceneManager.LoadSceneAsync(SceneName);
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        return ScriptPlayable<LoadScene>.Create(graph);
    }

    public void OnGraphStop(Playable playable)
    {
    }

    public void OnPlayableCreate(Playable playable)
    {
    }

    public void OnPlayableDestroy(Playable playable)
    {
    }

    public void OnBehaviourPause(Playable playable, FrameData info)
    {
    }

    public void PrepareFrame(Playable playable, FrameData info)
    {
    }

    public void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
    }
}
