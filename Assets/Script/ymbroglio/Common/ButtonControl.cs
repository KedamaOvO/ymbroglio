﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControl : MonoBehaviour {

    public bool TouchToTrigger = true;
    public TriggerBase InteractiveObj;
    // Use this for initialization
    public bool UntouchToOff;
    public bool IsOn=false;
	void Start () {
		
	}
	


	// Update is called once per frame
	void Update () {
		
	}

    
    private void OnTriggerEnter(Collider other)
    {
        if (TouchToTrigger && !IsOn)
        {
            IsOn = true;
            DoTrigger();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (TouchToTrigger && !IsOn)
        {
            IsOn = true;
            DoTrigger();
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (UntouchToOff && IsOn)
        {
            IsOn = false;
            DoTriggerOff();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (UntouchToOff && IsOn)
        {
            IsOn = false;
            DoTriggerOff();
        }
    }

    public void DoTrigger()
    {
        print("touched");
        InteractiveObj.TriggerOn();
    }

    public void DoTriggerOff()
    {
        print("triggerOff");
        InteractiveObj.TriggerOff();
    }
}
