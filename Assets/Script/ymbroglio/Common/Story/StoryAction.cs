﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class StoryAction {
	
	public int CharacterId;
	public string audio;
	public int AniState;
	public int LookAtCharacterId;
	public int DurationMs;
	public string Sentence;
}
