﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Story
{
    public class StoryDialog2D : MonoBehaviour
    {
        public string DisplayName;

        public Transform BindingObject;
        public RectTransform DiglogObject;
        public Text CharacterNameUI;
        public Text TextUI;
        public Vector3 PositionOffset;

        public virtual string Text
        {
            get
            {
                return TextUI.text;
            }

            set
            {
                TextUI.text = value;
            }
        }

        public void Start()
        {
            DisplayName = string.IsNullOrEmpty(DisplayName) ? BindingObject.name : DisplayName;
            CharacterNameUI.text = DisplayName;
        }

        public virtual void Show()
        {
            DiglogObject.gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            DiglogObject.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            var pos = BindingObject.position;
            pos += PositionOffset;
            DiglogObject.position = Camera.main.WorldToScreenPoint(pos);
        }
    }
}