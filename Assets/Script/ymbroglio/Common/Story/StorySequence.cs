﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Story
{
    [Serializable]
    public class StoryAction
    {
        public string CharacterName;//Key
        public string Text;
    }

    public class StorySequence:MonoBehaviour
    {
        public List<StoryAction> Actions;
    }
}

