﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Story.Test
{
    public class StoryPointTrigger : MonoBehaviour
    {
        private StorySequence storySequence;

        private void Start()
        {
            storySequence = GetComponent<StorySequence>();
        }

        private void OnTriggerEnter(Collider other)
        {
            InputManager.SetInputFlags(InputFlags.Stroy);
            InputManager.ResetInputFlags(InputFlags.Game);

            StoryManager.Instance.Sequence = storySequence;
            StoryManager.Instance.StartStory();
            StoryManager.Instance.OnStopOnce(() =>
            {
                InputManager.SetInputFlags(InputFlags.Game);
                InputManager.ResetInputFlags(InputFlags.Stroy);
            });
        }
    }
}
