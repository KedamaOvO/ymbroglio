﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Story
{
    public class StoryManager : MonoBehaviour
    {
        [Serializable]
        public class StoryDialogPair
        {
            public GameObject Character;
            public StoryDialog2D StoryDialog;
        }

        public List<StoryDialogPair> StoryDialogs;
        public Dictionary<string, StoryDialog2D> StoryDialogDictionary;
        public StorySequence Sequence { get; set; }
        public int ActionIndex =0;


        private Queue<Action> actionQueue = new Queue<Action>();
        private InputManager inputManager=new InputManager(InputFlags.Stroy);

        private bool _isPlaying = false;
        public bool IsPlaying
        {
            get { return _isPlaying; }
            private set { _isPlaying = true; }
        }

        public static StoryManager Instance;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Debug.unityLogger.Log("StoryManager存在两个及其两个以上实例");

            StoryDialogDictionary = StoryDialogs.ToDictionary((s) => s.Character.name, s => s.StoryDialog);
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        public void OnStopOnce(Action action)
        {
            actionQueue.Enqueue(action);
        }

        public void StartStory()
        {
            ActionIndex  = 0;
            IsPlaying = true;

            NextStoryAction();
        }

        public void PauseStory()
        {
            IsPlaying = false;
        }

        public void StopStory()
        {
            ActionIndex  = Sequence.Actions.Count;
            IsPlaying = false;

            foreach(var @char in StoryDialogs)
            {
                @char.StoryDialog.Hide();
            }

            while(actionQueue.Count!=0)
                actionQueue.Dequeue()();
        }

        private void NextStoryAction()
        {
            if (ActionIndex >= Sequence.Actions.Count)
            {
                StopStory();
                return;
            }

            StoryAction action = Sequence.Actions[ActionIndex];
            StoryDialog2D control = StoryDialogDictionary[action.CharacterName];
            StoryAction lastAction = Sequence.Actions[Mathf.Max(ActionIndex - 1, 0)];
            StoryDialog2D lastControl = StoryDialogDictionary[lastAction.CharacterName];

            if (ActionIndex != 0)
            {
                if (lastControl.name!= control.name)
                {
                    lastControl.Hide();
                    control.Show();
                }
            }
            else
            {
                control.Show();
            }

            control.Text = action.Text;

            ActionIndex++;
        }

        // Update is called once per frame
        void Update()
        {
            if (!IsPlaying) return;

            if(inputManager.GetButtonDown("NextStoryAction"))
            {
                NextStoryAction();
            }
        }
    }
}


