﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveTrigger : TriggerBase {
    public override void TriggerOff()
    {

    }

    public override void TriggerOn()
    {
        gameObject.SetActive(true);
    }
}
