﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InactiveTrigger : TriggerBase {

    public override void TriggerOff()
    {
    }

    public override void TriggerOn()
    {
        this.gameObject.SetActive(false);
    }
}
