﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatPlatformTrigger : TriggerBase {

    
    public Vector3 RelativeTargetPosition;

    public float MoveDuration=2f;
	private Vector3 targetPosition;
    Vector3 originalPosition;

	public bool IsMoving;
	public new AudioClip audio;
	// Use this for initialization
	void Start () {       
        originalPosition = transform.localPosition;
		targetPosition= originalPosition + RelativeTargetPosition;
}



public override void TriggerOn()
    {
		if(audio!=null)
			GameManager.Instance.GameAudioSource.PlayOneShot(audio);
		IsMoving = true;

		gameObject.transform.DOLocalMove(targetPosition, MoveDuration).SetLoops(1).OnComplete(delegate() { IsMoving = false; });
    }

    public override void TriggerOff()
    {
		if (audio != null)
			GameManager.Instance.GameAudioSource.PlayOneShot(audio);
		IsMoving = true;
		gameObject.transform.DOLocalMove(originalPosition, MoveDuration).SetLoops(1).OnComplete(delegate () { IsMoving = false; });
	}

    // Update is called once per frame
    void Update () {
		
	}

	void TweenComplete()
	{
		IsMoving = false;
	}
}
