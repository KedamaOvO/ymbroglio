﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Flags]
public enum InputFlags : UInt32
{
    Game = 1 << 0,
    UI = 1 << 1,
    Stroy = 1 << 2
};

public class InputManager{
    private static InputFlags s_inputFlags=InputFlags.Game;
    private InputFlags m_flag;
    
    public InputManager(InputFlags flag)
    {
        m_flag = flag;
    }

    public static void SetInputFlags(InputFlags flags)
    {
        s_inputFlags |= flags;
    }

    public static void ResetInputFlags(InputFlags flags)
    {
        s_inputFlags &= ~flags;
    }

    public bool GetButton(string key)
    {
        if ((s_inputFlags & m_flag) == 0) return false;
        return Input.GetButton(key);
    }

    public bool GetButtonUp(string key)
    {
        if ((s_inputFlags & m_flag) == 0) return false;
        return Input.GetButtonUp(key);
    }

    public bool GetButtonDown(string key)
    {
        if ((s_inputFlags & m_flag) == 0) return false;
        return Input.GetButtonDown(key);
    }

    public bool GetKey(KeyCode key)
    {
        if ((s_inputFlags & m_flag) == 0) return false;
        return Input.GetKey(key);
    }

    public bool GetKeyUp(KeyCode key)
    {
        if ((s_inputFlags & m_flag) == 0) return false;
        return Input.GetKeyUp(key);
    }

    public bool GetKeyDown(KeyCode key)
    {
        if ((s_inputFlags & m_flag) == 0) return false;
        return Input.GetKeyDown(key);
    }

    public float GetAxis(String AxisName)
    {
        if ((s_inputFlags & m_flag) == 0) return 0;
        return Input.GetAxis(AxisName);
    }
}
