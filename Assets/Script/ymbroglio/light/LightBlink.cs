﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBlink : MonoBehaviour {

    public float blinkDuration=1f;
    public float DarkFactor = 0.3f;
    public float RandomFactor = 0.5f;
    new Light light;
	// Use this for initialization
	void Start () {
        SwitchLight();

    }

    private void Awake()
    {
        light = GetComponent<Light>();
    }

    void SwitchLight()
    {
        float nextTime;
        if(light.enabled)
        {
            nextTime = blinkDuration * Random.Range(-RandomFactor, RandomFactor);
            
        }
        else
        {
            nextTime = blinkDuration * Random.Range(-RandomFactor, RandomFactor)*DarkFactor;
            
        }
        light.enabled = !light.enabled;
        Invoke("SwitchLight", nextTime);

    }
    // Update is called once per frame
    void Update () {
		
	}
}
