﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{


	/// <summary>
	/// 0 TITLE , 1 GAME RUN 2 GAME OVER 3 MENU
	/// </summary>
	public int GameState;

	public bool IsEnergyOn = false;

	public GameObject VCam_Z;
	public GameObject VCam_X;
	public GameObject VCam_NZ;

	public GameObject UIGameOver;

	public GameObject GameOverPostProcessing;

	public GameObject Player;

	public GameObject Drone;

	public GameObject MainCamera;

	public AudioSource GameAudioSource;

    public Image image; 

	public GameObject DefaultObjectContainer;

	public bool RayMouseMode;

	//X Z NX NZ
	public string CurrentViewMode;
	// Use this for initialization
	void Start()
	{
        Debug.Log("SetCurView");
        

        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
	}

	private void Awake()
	{
		Instance = this;
        CurrentViewMode = "X";
    }

	public static GameManager Instance;

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey(KeyCode.R))
		{
			SwitchViewMode("Z", 78f);
		}
		else if (Input.GetKey(KeyCode.T))
		{
			SwitchViewMode("X", -2.48f);
		}
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="viewMode"></param>
	/// <param name="pos">
	/// X NX  pos is z
	/// Z NZ pos is x
	/// </param>
	public void SwitchViewMode(string newViewMode, float pos)
	{
        Debug.Log("SwitchView");
		if (CurrentViewMode == "X" && newViewMode == "Z")
		{
			VCam_X.SetActive(true);
			VCam_Z.SetActive(false);
			Player.transform.DOMoveX(pos, 0.2f);
			Drone.transform.DOMoveX(pos, 1f);
			Drone.transform.DOMoveZ(0.8f, 0.2f).SetRelative();
			Player.transform.DORotate(new Vector3(0, 0, 0), 0.8f);
			Drone.transform.DORotate(new Vector3(0, -180, 0), 0.8f);
			Player.GetComponent<PlayerControl>().m_FacingRight = true;
			Drone.GetComponent<DroneControl>().facingRight = true;
			CurrentViewMode = newViewMode;
            Debug.Log("NewView" + newViewMode);

            Debug.Log("CurView"+ CurrentViewMode);

			SetPlayerContrain();
            Debug.Log("XtoZ");
		}
		else if (CurrentViewMode == "Z" && newViewMode == "X")
		{
			VCam_Z.SetActive(true);
			VCam_X.SetActive(false);

			Player.transform.DORotate(new Vector3(0, 270, 0), 0.8f);
			Drone.transform.DORotate(new Vector3(0, 90, 0), 0.8f);
			Player.transform.DOMoveZ(pos, 0.2f);
			Drone.transform.DOMoveZ(pos, 0.2f);
			Drone.transform.DOMoveX(-0.8f, 1f).SetRelative();

			Player.GetComponent<PlayerControl>().m_FacingRight = false;
			Drone.GetComponent<DroneControl>().facingRight = false;
			CurrentViewMode = newViewMode;
			SetPlayerContrain();
		}
		else if (CurrentViewMode == "Z" && newViewMode == "NX")
		{
			VCam_NZ.SetActive(true);
			VCam_X.SetActive(false);

			Player.transform.DOLocalRotate(new Vector3(0, -90, 0), 0.8f).SetRelative();
			Drone.transform.DOLocalRotate(new Vector3(0, -90, 0), 0.8f).SetRelative();
			Player.transform.DOMoveZ(pos, 0.2f);
			Drone.transform.DOMoveZ(pos, 0.2f);
			Drone.transform.DOMoveX(-0.8f, 1f).SetRelative();

			Player.GetComponent<PlayerControl>().m_FacingRight = true;
			//Drone.GetComponent<DroneControl>().facingRight = true;
			CurrentViewMode = newViewMode;
			SetPlayerContrain();
		}
		else if (CurrentViewMode == "NX" && newViewMode == "Z")
		{
			VCam_X.SetActive(true);
			VCam_NZ.SetActive(false);
			Player.transform.DOMoveX(pos, 0.2f);
			Drone.transform.DOMoveX(pos, 1f);
			Drone.transform.DOMoveZ(0.8f, 0.2f).SetRelative();
			Player.transform.DOLocalRotate(new Vector3(0, 90, 0), 0.8f).SetRelative();
			Drone.transform.DOLocalRotate(new Vector3(0, 90, 0), 0.8f).SetRelative();
			Player.GetComponent<PlayerControl>().m_FacingRight = false;
			//Drone.GetComponent<DroneControl>().facingRight = false;
			CurrentViewMode = newViewMode;
			SetPlayerContrain();
		}
        Debug.Log("CurView2" + CurrentViewMode);
    }

	public void SetPlayerContrain()
	{
		if (CurrentViewMode == "X" || CurrentViewMode == "NX")
		{
            Debug.Log("Freeze" + CurrentViewMode);
			Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;
			Drone.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;

		}
		else if (CurrentViewMode == "Z" || CurrentViewMode == "NZ")
		{
            Debug.Log("Freeze" + CurrentViewMode);
            Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX;
            Debug.Log("Rigid" + Player.GetComponent<Rigidbody>().constraints);
			Drone.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX;
            Debug.Log("Rigid" + Drone.GetComponent<Rigidbody>().constraints);
        }
	}


	public void GameOver()
	{
        image.gameObject.SetActive(true);
        image.DOFade(1, 2f).SetUpdate(true).OnComplete(()=> {
            ResetSystem.Instance.ResetPlayer();
            image.gameObject.SetActive(false);
        });
        image.color = new Color(0,0,0,0);
        //UIGameOver.SetActive(true);
        //GameOverPostProcessing.SetActive(true);
        //Player.GetComponent<SignalConrtol>().DisplayerSignal = false;
        //Player.GetComponent<SignalConrtol>().m_lineManager.gameObject.SetActive(false);   
	}


	public void BackToTitle()
	{

	}

	public void RestartGame()
	{
        ResetSystem.Instance.ResetPlayer();
        GameOverPostProcessing.SetActive(false);
        UIGameOver.SetActive(false);
    }


}
