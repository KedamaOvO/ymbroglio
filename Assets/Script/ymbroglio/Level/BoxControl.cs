﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxControl : MonoBehaviour {

	public bool DroneInScope;

	public bool PlayerInScope;

	public bool IsDroneTake;

	public bool IsPlayerTake;

	public KeyCode DroneInteractiveButton=KeyCode.L;

	public KeyCode PlayerInterActiveButton=KeyCode.P;

	public float BoxHeight=1;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update()
	{
		if (!IsPlayerTake && !IsDroneTake)
		{
			if (PlayerInScope && !IsDroneTake)
			{
				if (Input.GetKeyDown(PlayerInterActiveButton) || Input.GetButtonDown("Fire2"))
				{
					this.transform.parent = GameManager.Instance.Player.transform;
					this.GetComponent<Rigidbody>().isKinematic = true;
					this.GetComponent<BoxCollider>().isTrigger = true;
					IsPlayerTake = true;
				}


			}

			if (DroneInScope && !IsPlayerTake)
			{
				if (Input.GetKeyDown(DroneInteractiveButton) || Input.GetButtonDown("Fire2"))
				{
					this.transform.parent = GameManager.Instance.Drone.transform;
					this.GetComponent<Rigidbody>().isKinematic = true;
					this.GetComponent<BoxCollider>().isTrigger = true;
					ChangeDroneCollider(this.BoxHeight);
					IsDroneTake = true;
				}

			}
		}

		if (IsPlayerTake)
		{
			if (Input.GetKeyUp(PlayerInterActiveButton)|| Input.GetButtonUp("Fire2"))
			{
				this.transform.parent = GameManager.Instance.DefaultObjectContainer.transform;
				this.GetComponent<Rigidbody>().isKinematic = false;
				this.GetComponent<BoxCollider>().isTrigger = false;
				IsPlayerTake = false;
			}

		}
		else if (IsDroneTake)
		{
			if (Input.GetKeyUp(DroneInteractiveButton) || Input.GetButtonUp("Fire2"))
			{
				this.transform.parent = GameManager.Instance.DefaultObjectContainer.transform;
				this.GetComponent<Rigidbody>().isKinematic = false;
				this.GetComponent<BoxCollider>().isTrigger = false;
				ChangeDroneCollider(-this.BoxHeight);
				IsDroneTake = false;
			}
		}

		
	}

	public void ChangeDroneCollider(float boxheight)
	{
		boxheight = boxheight / GameManager.Instance.Drone.transform.localScale.y;
		CapsuleCollider collider=GameManager.Instance.Drone.GetComponent<CapsuleCollider>();
		Vector3 center = collider.center;
		collider.center = new Vector3(center.x, center.y - boxheight / 2, center.z);
		collider.height = collider.height + boxheight;
	}

	public void OnCollisionEnter(Collision collision)
	{
		if (IsPlayerTake || IsDroneTake) return;
		GameObject collObj = collision.gameObject;
		if (collObj.GetComponent<PlayerControl>() != null)
		{
			PlayerInScope = true;
		}
		else if (collObj.GetComponent<DroneControl>() != null)
		{
			if (CheckDroneOnTop())
				DroneInScope = true;

		}
	}

	public bool CheckDroneOnTop()
	{
		RaycastHit hitInfo;
		if (Physics.Raycast(transform.position, Vector3.up, out hitInfo, 2f))
		{
			if (hitInfo.collider.gameObject.GetComponent<DroneControl>() != null) return true;
			else return false;
		}
		else return false;
	}
	public void OnCollisionExit(Collision collision)
	{
		if (IsPlayerTake || IsDroneTake) return;
		GameObject collObj = collision.gameObject;
		if (collObj.GetComponent<PlayerControl>() != null)
		{
			PlayerInScope = false;
		}
		else if (collObj.GetComponent<DroneControl>() != null)
		{
			DroneInScope = false;

		}
	}
}
