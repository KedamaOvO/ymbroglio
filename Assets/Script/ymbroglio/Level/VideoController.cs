﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoController : MonoBehaviour {

    public GameObject loading;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerControl>() != null)
        {
            loading.gameObject.SetActive(true);
            SceneManager.LoadSceneAsync("TRANSITION");
        }
    }
}
