﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeControl : TriggerBase {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void TriggerOn()
    {
        gameObject.SetActive (true);
    }

    public override void TriggerOff()
    {
        gameObject.SetActive(false);
    }
}
