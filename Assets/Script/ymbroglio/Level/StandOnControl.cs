﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(FloatPlatformTrigger))]
public class StandOnControl : MonoBehaviour {


	public bool IsPlayerOn;
	public GameObject Player;
	public Vector3 RelativePos;

	FloatPlatformTrigger moveTrigger;
	// Use this for initialization
	void Start () {
		moveTrigger = this.GetComponent<FloatPlatformTrigger>();
	}

	// Update is called once per frame
	void Update()
	{
		if (IsPlayerOn && moveTrigger.IsMoving)
		{
			Player.transform.position = this.transform.position + RelativePos;
		}
		else if (IsPlayerOn && !moveTrigger.IsMoving)
		{
			RelativePos = Player.transform.position - this.transform.position;
		}
	}
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.GetComponent<PlayerControl>() != null)
		{
			IsPlayerOn = true;
			Player = collision.gameObject;
			RelativePos = Player.transform.position - this.transform.position;
		}
	}

	private void OnCollisionExit(Collision collision)
	{
		if (collision.gameObject.GetComponent<PlayerControl>() != null)
		{
			IsPlayerOn = false;
		}
	}
}
