﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class DroneControl : MonoBehaviour
{


    [HideInInspector]
    public bool facingRight = true;         // For determining which way the player is currently facing.


	public float MaxEnergy = 100;
	public float CurrentEnergy = 100;
	public float LeekSpeed_FlyUp = 35;  //Fly up
	public float LeekSpeed_FlyMove = 25;  //Move left and right
	public float LeekSpeed_Idle = 6;
	public float LeekSpeed_Ground = 2;

	public float RechargeSpeed = 30;  //per secound

	public bool Recharge = false;
	public Text eneryTxt;

	public bool Leak = false;

	public bool jump = false;				// Condition for whether the player should jump.

    public float moveForce = 365f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 5f;             // The fastest the player can travel in the x axis.
    public AudioClip[] jumpClips;           // Array of clips for when the player jumps.

    public float jumpForce = 1000f;         // Amount of force added when the player jumps.

    public bool Grounded = false;
	public float GroundCheckDistance = 0.25f;
	private Animator anim;                  // Reference to the player's animator component.

                                            // Use this for initialization

    private Transform groundCheck;          // A position marking where to check if the player is grounded.

    public bool EnergyOn = false;
    public bool DebugMode = false;
    public float Speed = 2;

    public Material matNormal;
    public Material matNoPower;

    public GameObject DroneMesh;

	public GameObject Container;

    private InputManager inputManger = new InputManager(InputFlags.Game);

    void Awake()
    {
        // Setting up references.
        //groundCheck = transform.Find("groundCheck");
        anim = GetComponent<Animator>();
    }

	private void OnGUI()
	{
		if (eneryTxt != null)
		{
            eneryTxt.rectTransform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            eneryTxt.text = CurrentEnergy.ToString("0");
		}
		//eneryTxt.transform.parent.transform.LookAt(GameManager.Instance.MainCamera.transform);
	}

	void Start()
    {

    }

	// Update is called once per frame
	void Update()
	{
		/// if(GameManager.Instance.)
		/// 
		/*if (Vector3.Distance(this.transform.position, GameManager.Instance.Player.transform.position) < 1.6f)
		{
			Recharge = true;
		}
		else
		{
			Recharge = false;
		}*/




        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        //grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        // If the jump button is pressed and the player is grounded then the player should jump.
        //     if (Input.GetButtonDown("Jump") && grounded)
        //         jump = true;
        
	}

	void FixedUpdate()
    {
		CheckGroundStatus();
		if (GameManager.Instance.IsEnergyOn) Recharge = true;
		else Recharge = false;
		if (Recharge)
		{
			if (CurrentEnergy < MaxEnergy) { CurrentEnergy += RechargeSpeed * Time.deltaTime; }

			if (CurrentEnergy > MaxEnergy) CurrentEnergy = MaxEnergy;


		}
		if (CurrentEnergy <= 0 && !DebugMode)
        {
            this.GetComponent<Rigidbody>().useGravity = true;
            DroneMesh.GetComponent<SkinnedMeshRenderer>().material = matNoPower;
            return;
        }
        else
        {
            this.GetComponent<Rigidbody>().useGravity = true;
            DroneMesh.GetComponent<SkinnedMeshRenderer>().material = matNormal;
        }

        // Cache the horizontal input.
        float h = 0; //= Input.GetAxis("Horizontal");
        float v = 0;

		if (CurrentEnergy > 0)
		{
			if (inputManger.GetKey(KeyCode.RightArrow))
			{
				h = 1;
			}
			else if (inputManger.GetKey(KeyCode.LeftArrow))
			{
				h = -1;
			}
			else
			{
				h = inputManger.GetAxis("LeftX");
			}

			if (inputManger.GetKey(KeyCode.UpArrow))
			{
				v = 1;
			}
			else
			{
				v = inputManger.GetAxis("LeftY");
			}
			/*else if (Input.GetKey(KeyCode.DownArrow))
			{
				v = -1;
			}*/
		}
        //print(h);
        // The Speed animator parameter is set to the absolute value of the horizontal input.
        //anim.SetFloat("Speed", Mathf.Abs(h));


        if (h!=0 || v!=0)
        {
			if (GameManager.Instance.CurrentViewMode == "X")
			{
				GetComponent<Rigidbody>().velocity = new Vector3(h * Speed, v * Speed, 0);
			}
			else if (GameManager.Instance.CurrentViewMode == "Z")
			{
				GetComponent<Rigidbody>().velocity = new Vector3(0, v * Speed, h * Speed);
			}
			if (GameManager.Instance.CurrentViewMode == "NX")
			{
				GetComponent<Rigidbody>().velocity = new Vector3(-h * Speed, v * Speed, 0);
			}

		}
            /*
        if(v*GetComponent<Rigidbody2D>().velocity.y<maxSpeed)
        {
            // GetComponent<Rigidbody2D>().AddForce(Vector2.up * v * moveForce);
            GetComponent<Rigidbody2D>().velocity = Vector2.up*v*Speed;
        }
        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        else if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
        {
            // ... add a force to the player.
            //GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
            GetComponent<Rigidbody2D>().velocity = Vector2.right * h * Speed;
        }*/


        /*
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.y) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody2D>().velocity.x, Mathf.Sign(GetComponent<Rigidbody>().velocity.y) * maxSpeed,0);


        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            GetComponent<Rigidbody>().velocity = new Vector3(Mathf.Sign(GetComponent<Rigidbody>().velocity.x) * maxSpeed, GetComponent<Rigidbody>().velocity.y,0);
            */

        //print(GetComponent<Rigidbody>().velocity); 
        // If the input is moving the player right and the player is facing left...
        if (h > 0 && !facingRight)
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h < 0 && facingRight)
            // ... flip the player.
            Flip();

		// If the player should jump...


		if (Leak && !Recharge)
		{
			if (CurrentEnergy > 0) {
				if (v > 0)
				{
					CurrentEnergy -= LeekSpeed_FlyUp * Time.deltaTime;
				}
				else if (h != 0)
				{
					CurrentEnergy -= LeekSpeed_FlyMove * Time.deltaTime;
				}
				else if(groundCheck)
				{
					CurrentEnergy -= LeekSpeed_Ground * Time.deltaTime;
				}
				else 
				{
					CurrentEnergy -= LeekSpeed_Idle * Time.deltaTime;
				}


			}
			if (CurrentEnergy < 0) CurrentEnergy = 0;
		}
	}


    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;
		//print(facingRight);
		// Multiply the player's x local scale by -1.
		//Vector3 theScale = transform.localScale;
		//theScale.z *= -1;
		//transform.localScale = theScale;

		transform.localRotation = Quaternion.Euler(0, 180, 0) * transform.localRotation;
	}



	void CheckGroundStatus()
	{
		RaycastHit hitInfo;
#if UNITY_EDITOR
		// helper to visualise the ground check ray in the scene view
		//Debug.
		Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * GroundCheckDistance));
#endif
		// 0.1f is a small offset to start the ray from inside the character
		// it is also good to note that the transform position in the sample assets is at the base of the character
		if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, GroundCheckDistance))
		{

			Grounded = true;
			if (hitInfo.collider.gameObject.GetComponent<BoxControl>() != null)
			{
				hitInfo.collider.gameObject.GetComponent<BoxControl>().DroneInScope = true;
			}

		}
		else
		{
			Grounded = false;
		}
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent<TrapControl>() != null)
        {
            GameManager.Instance.GameOver();
        }
    }
}
