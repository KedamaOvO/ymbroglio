﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnViewTriggerControl : MonoBehaviour {

	//X Z NX NZ
	public string TurnToMode = "Z";

	// Z mode. it's x postion, X Mode it's z postion
	public float BasePosition;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.GetComponent<PlayerControl>()!=null) 
			GameManager.Instance.SwitchViewMode(TurnToMode, BasePosition);
	}
}
