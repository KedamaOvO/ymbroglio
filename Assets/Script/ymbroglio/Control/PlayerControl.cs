﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class PlayerControl : MonoBehaviour
{

	[SerializeField] float m_JumpPower = 12f;
	//[Range(1f, 4f)] [SerializeField] float m_GravityMultiplier = 2f;
	[SerializeField] float m_MoveSpeedMultiplier = 1f;
	[SerializeField] float m_ClimbSpeedMultiplier = 0.5f;
	[SerializeField] float m_MoveMaxSpeed = 5f;
	[SerializeField] float m_MoveForce = 5f;
	[SerializeField] float m_AnimSpeedMultiplier = 1f;
	[SerializeField] float m_GroundCheckDistance = 0.3f;
	[SerializeField] float m_RunCycleLegOffset = 0.2f;

	Rigidbody m_Rigidbody;
	Animator m_Animator;
	public bool m_IsGrounded = true;
	float m_OrigGroundCheckDistance;
	const float k_Half = 0.5f;
	float m_TurnAmount;
	float m_ForwardAmount;
	Vector3 m_GroundNormal;
	float m_CapsuleHeight;
	Vector3 m_CapsuleCenter;
	CapsuleCollider m_Capsule;
	bool m_Crouching;
	bool m_Climbing;
	public bool m_FacingRight = true;
    private InputManager inputManger = new InputManager(InputFlags.Game);
    // Use this for initialization

    public GameObject Container;

	private GameObject m_CurrentLedder;

	void Start()
	{
		m_Animator = GetComponent<Animator>();
		m_Rigidbody = GetComponent<Rigidbody>();
		m_Capsule = GetComponent<CapsuleCollider>();
		m_CapsuleHeight = m_Capsule.height;
		m_CapsuleCenter = m_Capsule.center;
		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;
		m_OrigGroundCheckDistance = m_GroundCheckDistance;
		m_Climbing = false;
	}

	// Update is called once per frame
	void Update()
	{
		//print(Input.GetAxis("Horizontal"));
	}



	void FixedUpdate()
	{
		if (GetComponent<Rigidbody>().velocity.y <= 0)
			CheckGroundStatus();
		CheckClimbStatus();
		bool jump = false;
		bool crouch = false;
		bool climb = false;
		// Cache the horizontal input.
		float h = 0; //= Input.GetAxis("Horizontal");
		float v = 0;

		climb = m_Climbing;
		if (inputManger.GetKey(KeyCode.D))
		{
			h = 1;
		}
		else if (inputManger.GetKey(KeyCode.A))
		{
			h = -1;
		}
		else
		{
			h = inputManger.GetAxis("Horizontal");
		}

		if (climb)
		{
			if (inputManger.GetKey(KeyCode.W)) v = 1;
			else if (inputManger.GetKey(KeyCode.S)) v = -1;

		}
		if ((inputManger.GetKeyDown(KeyCode.Space)|| inputManger.GetButtonDown("Jump"))     && m_IsGrounded && !m_Climbing)
        {
            jump = true;
			m_Animator.applyRootMotion = true; ;
		}
        else if (inputManger.GetKey(KeyCode.C) && m_IsGrounded && !m_Climbing)
        {
            crouch = true;
        }
        Vector2 moveVector;
        moveVector = new Vector2(h, jump ? 1 : v);


        Move(moveVector, crouch, jump, climb);
        /*
        if (Input.GetKeyDown(KeyCode.E))
        {
            print((Vector3.Distance(this.transform.position, MyBox.transform.position)));
            if (Vector3.Distance(this.transform.position, MyBox.transform.position) < 4)
            {
                GrabBox();
            }
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            ReleaseBox();
        }*/

        // The Speed animator parameter is set to the absolute value of the horizontal input.



    }

    private void Move(Vector2 move, bool crouch, bool jump, bool climb)
    {
        float h = move.x;
        float v = move.y;

		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		//if (h * GetComponent<Rigidbody>().velocity.x < m_MoveMaxSpeed)
		// ... add a force to the player.

		float movex=0;
		float movez=0;
		if (GameManager.Instance.CurrentViewMode == "X")
		{
			movex = h;
		}
		else if(GameManager.Instance.CurrentViewMode == "Z")
		{
			movez = h;
		}
		else if (GameManager.Instance.CurrentViewMode == "NX")
		{
			movex = -h;
		}


		if (!climb)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(movex * m_MoveSpeedMultiplier, GetComponent<Rigidbody>().velocity.y, movez * m_MoveSpeedMultiplier); 
        }
        else
        {
            GetComponent<Rigidbody>().velocity = new Vector3(movex * m_MoveSpeedMultiplier, v * m_ClimbSpeedMultiplier, movez * m_MoveSpeedMultiplier);
        }
        // If the player's horizontal velocity is greater than the maxSpeed...
        //if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) > m_MoveMaxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
         //   GetComponent<Rigidbody>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody>().velocity.x) * m_MoveMaxSpeed, GetComponent<Rigidbody>().velocity.y);

        // If the input is moving the player right and the player is facing left...
        if (h > 0 && !m_FacingRight)
            // ... flip the player.
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h < 0 && m_FacingRight)
            // ... flip the player.
            Flip();

        //print(jump);

        if (v>0.1 && m_IsGrounded )
        {
            m_IsGrounded = false;
            GetComponent<Rigidbody>().AddForce(Vector3.up * m_JumpPower);
        }

        UpdateAnimator(move, crouch, jump, climb);
        // If the player should jump...
        /*
        if (jump)
        {
            // Set the Jump animator trigger parameter.
            anim.SetTrigger("Jump");

            // Play a random jump audio clip.
            int i = Random.Range(0, jumpClips.Length);
            //AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);

            // Add a vertical force to the player.
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));

            // Make sure the player can't jump again until the jump conditions from Update are satisfied.
            jump = false;
        }*/
    }

    void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;
        //print(m_FacingRight);
        // Multiply the player's x local scale by -1.
        //Vector3 theScale = transform.localScale;
        //theScale.z *= -1;
        //transform.localScale = theScale;

        transform.localRotation = Quaternion.Euler(0, 180, 0) * transform.localRotation;
    }

    void FlipToRight()
    {
        if (!m_FacingRight) Flip();
    }
    void FlipToLeft()
    {
        if (m_FacingRight) Flip();
    }


    void UpdateAnimator(Vector2 move, bool crouch, bool jump, bool climb)
    {
        float h = move.x;
        float v = move.y;

		if (!m_IsGrounded)
		{
			m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
		}

		float runCycle =
	Mathf.Repeat(
		m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
		float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
		if (m_IsGrounded)
		{
			m_Animator.SetFloat("JumpLeg", jumpLeg);
		}

		if (!climb)
        {
            //m_Animator.speed = 1;
            m_Animator.SetBool("Climb", false);
            m_Animator.SetFloat("Forward", Mathf.Abs(h));
            m_Animator.SetBool("OnGround", m_IsGrounded);
            m_Animator.SetBool("Crouch", crouch);

        }
        else
        {
            m_Animator.SetFloat("Forward", Mathf.Abs(v));
            m_Animator.SetBool("Climb", true);
            //if (v == 0) m_Animator.speed = 0.3f;
            //else m_Animator.speed = 1f;
        }
        //m_Animator.SetFloat("Jump", jump ? 1 : 0);


    }


    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        //Debug.
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
        {
            m_GroundNormal = hitInfo.normal;
            m_IsGrounded = true;
            m_Animator.applyRootMotion = false;

        }
        else
        {
            m_IsGrounded = false;
            m_GroundNormal = Vector3.up;
            m_Animator.applyRootMotion = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.GetComponent<LadderControl>()!=null)
        {
            Bounds ladderBounds=collision.collider.gameObject.GetComponent<BoxCollider>().bounds;
            if (ladderBounds.max.y + 0.1 > this.transform.position.y)
            {
                float contact_x = collision.contacts[0].point.x;
                if (contact_x < this.transform.position.x) FlipToLeft();
                else FlipToRight();

                ClimbOn(collision.collider.gameObject);
            }
        }
        else if (collision.collider.gameObject.GetComponent<TrapControl>() != null)
        {
            GameManager.Instance.GameOver();
        }
    }

    private void CheckClimbStatus() 
    {
        if(m_Climbing)
        {
            if (m_CurrentLedder.GetComponent<BoxCollider>().bounds.max.y -0.1f< this.transform.position.y)
            {
                ClimbOff();
            }

        }
    }


    private void ClimbOn(GameObject ledder)
    {
        m_CurrentLedder = ledder;
        m_Climbing = true;
        this.GetComponent<CapsuleCollider>().radius = 0.5f;
        GetComponent<Rigidbody>().useGravity = false;
        
    }

    private void ClimbOff()
    {
        m_CurrentLedder = null;
        m_Climbing = false;
        this.GetComponent<CapsuleCollider>().radius = 0.3f;
        GetComponent<Rigidbody>().useGravity = true;


    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent<LadderControl>() != null)
        {
            ClimbOff();
        }
    }
}
