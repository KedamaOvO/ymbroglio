﻿using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIKControl : MonoBehaviour {
	public GameObject aimTarget;
	public GameObject RaySource;
	public EnergyRay energyRay;


	public AimIK aimIk;
	// Use this for initialization
	void Start () {
		aimIk = GetComponent<AimIK>();
		energyRay = RaySource.GetComponent<EnergyRay>();
	}
	
	// Update is called once per frame
	void Update () {
		if (energyRay.DisplayRays)
		{
			aimIk.enabled = true;
			var pos = Camera.main.WorldToScreenPoint(RaySource.transform.position);

			
			Vector3 dir;
			if (GameManager.Instance.RayMouseMode)
			{
				Ray mouseRay=Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hitInfo;
				if(Physics.Raycast(mouseRay, out hitInfo))
					if (hitInfo.collider.gameObject.GetComponent<PlayerControl>() != null) return;
				//print(Vector3.Distance(Input.mousePosition, pos));
				//if (Vector3.Distance(Input.mousePosition, pos) < 15) return;
				dir = Input.mousePosition - pos;
				pos = RaySource.transform.position + dir.normalized*10;
			}
			else
			{
				float rightx = Input.GetAxis("RightX");
				float righty = Input.GetAxis("RightY");
				dir = new Vector3(rightx, righty, 0);
				pos = RaySource.transform.position + dir.normalized*100;
			}

			if (dir == Vector3.zero) aimIk.enabled = false;
			else
			{
				aimIk.enabled = true;
				this.GetComponent<SimpleAimingSystem>().enabled = true;
				aimTarget.transform.position = pos;
			}
		}
		else
		{
			aimIk.enabled = false;
			
			this.GetComponent<SimpleAimingSystem>().enabled=false;
			Animator animator = this.GetComponent<Animator>();
			animator.SetFloat("Aim Forward", 1f);
			animator.SetFloat("Aim Right", 0f);
			animator.SetFloat("Aim Left", 0f);
			animator.SetFloat("Aim Back", 0f);
			animator.SetFloat("Aim Up", 0f);
			animator.SetFloat("Aim Down", 0f);

		}


	}
}
