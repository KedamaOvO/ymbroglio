﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DebugControlBoard))]
public class DebugControlBoardEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		DebugControlBoard myScript = (DebugControlBoard)target;
		if (GUILayout.Button("Test"))
		{
			myScript.Test();
		}

		if (GUILayout.Button("Say words"))
		{
			myScript.Sayword();
		}
	}
}
