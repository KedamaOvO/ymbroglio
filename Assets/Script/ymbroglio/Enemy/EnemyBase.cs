﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public class EnemyBase : MonoBehaviour
    {
        public static EnemyBase CurrentStageBoss;

        public int MaxHealthPoint;
        public int HealthPoint;
    }
}
