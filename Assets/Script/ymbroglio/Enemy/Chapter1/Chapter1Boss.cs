﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Enemy
{
    public class Chapter1Boss : EnemyBase
    {
        public SwitchSystem SwitchSystem;

        [Header("BossStage")]
        public List<BossStageSetting> Stages;
        private List<GunController> m_guns;
        private List<string> m_GuninitialStateJsons;

        [Serializable]
        public class BulletSetting
        {
            public GameObject BulletObject;
            public float Interval;
            public bool CanAttack=true;
        }

        [Serializable]
        public class GunSetting
        {
            public string Name = "Gun";
            public GunController GunController;
            public float Delay = 0;
            public BulletSetting BulletSetting;
        }

        [Serializable]
        public class BossStageSetting
        {
            public string Name = "BossStageSetting";
            public List<GunSetting> GunSettings;
        }

        private void Awake()
        {
            EnemyBase.CurrentStageBoss = this;
            base.MaxHealthPoint = 4;
            base.HealthPoint = MaxHealthPoint;

            SwitchSystem.orderArray = RandomSwitchOrder();

            IEnumerable<GunController> list=new List<GunController>();

            foreach(var stage in Stages)
            {
                list=list.Union(stage.GunSettings.Select(g => g.GunController));
            }
            m_guns = list.Distinct().ToList();

            m_GuninitialStateJsons = m_guns.Select(gun => JsonUtility.ToJson(gun)).ToList();
        }

        private int m_lastHp = 0;

        private BossStageSetting _currentStage
        {
            get
            {
                return Stages[Math.Min(SwitchSystem.counter, Stages.Count - 1)]; ;
            }
        }

        private void FixedUpdate()
        {
            //if (SwitchSystem.counter == 0 && HealthPoint != MaxHealthPoint)
            //    SwitchSystem.orderArray = RandomSwitchOrder();

            base.HealthPoint = MaxHealthPoint - SwitchSystem.counter;

            if (m_lastHp != HealthPoint)
            {
                BossStageSetting stage = _currentStage;
                ResetAllGun();
                ChangeBossStage(stage);
            }

            m_lastHp = HealthPoint;
        }

        private void ResetAllGun()
        {
            for(int i=0;i<m_GuninitialStateJsons.Count; i++)
            {
                JsonUtility.FromJsonOverwrite(m_GuninitialStateJsons[i], m_guns[i]);
            }
        }

        private void ChangeBossStage(BossStageSetting stage)
        {
            foreach (var gun in stage.GunSettings)
            {
                StartCoroutine(ApplyGun(gun, gun.Delay));
            }
        }

        private IEnumerator ApplyGun(GunSetting gun,float time)
        {
            if(time>10e-6)
            yield return new WaitForSeconds(time);

            if (gun.Name.ToLower().StartsWith("goto first"))
            {
                print("GotoFrist");
                ResetAllGun();
                ChangeBossStage(_currentStage);
            }
            else
            {
                try
                {
                    if (gun.BulletSetting.BulletObject != null)
                        gun.GunController.bullet = gun.BulletSetting.BulletObject;
                    gun.GunController.timeBetweenAttack = gun.BulletSetting.Interval;
                    gun.GunController.canAttack = gun.BulletSetting.CanAttack;
                }
                catch(NullReferenceException)
                {
                    Debug.unityLogger.LogError("Chapter1Boss",string.Format("Name:{0} Stage:{1}", gun.Name, Stages[MaxHealthPoint - HealthPoint].Name));
                }

            }
        }

        private int[] RandomSwitchOrder()
        {
            int[] order = { 0, 1, 2, 3 };
            for (int i = 0; i < 6; i++)
            {
                int j = UnityEngine.Random.Range(0, 4);
                int k = UnityEngine.Random.Range(0, 4);
                int t = order[j];
                order[j] = order[k];
                order[k] = t;
            }
            return order;
        }
    }
}


