﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class ButtonDownEffect : MonoBehaviour,IPointerEnterHandler, IPointerExitHandler
{
    private Image image;

    public Sprite NormalImage;
    public Sprite EnterImage;
    public Vector3 Scale = new Vector3(0.8f, 0.8f, 1);

    public float Duration = 0.5f;

    private RectTransform rectTransform;

    private bool revers = false;
    private bool completed = true;

    public void OnPointerEnter(PointerEventData eventData)
    {
        revers = false;
        completed = false;
        if(EnterImage!=null)
            image.sprite = EnterImage;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        revers = true;
        completed = false;
        if (NormalImage!=null)
            image.sprite = NormalImage;
    }

    // Use this for initialization
    void Start () {
        rectTransform=GetComponent<RectTransform>();
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!completed)
        {
            if (!revers)
            {
                rectTransform.DOScale(new Vector3(1, 1, 1), Duration)
                    .OnComplete(() => completed = true)
                    .Play();
            }
            else
            {
                rectTransform.DOScale(Scale, Duration)
                    .OnComplete(() => completed = true)
                    .Play();
            }
        }
	}
}
