﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SequenceAnimation : MonoBehaviour {

    public Sprite[] Sprites;
    public int FPS = 25;
    public int Count;
    public int StartIndex = 0;
    public int Digit = 5;

    public string Path;
    public string SpritePrefix;

    private Image image;

    private int currentIndex = 0;
    private float nextTime = 0;
    private float timeStep = 0;

    // Use this for initialization
    void Start () {
        image = GetComponent<Image>();
        Sprites = new Sprite[Count];

        timeStep = 1.0f / FPS;
	}

    private void OnEnable()
    {
        currentIndex = 0;
    }

    private void Update()
    {
        if(Time.time>nextTime)
        {
            if (Sprites[currentIndex] == null)
            {
                Sprites[currentIndex] = Resources.Load<Sprite>(string.Format("{0}/{1}{2:D" + Digit + "}", Path, SpritePrefix, StartIndex + currentIndex).ToLower());
            }
            image.sprite = Sprites[currentIndex];
            currentIndex = (currentIndex + 1) % Count;
            nextTime = Time.time + timeStep;
        }
    }
}
