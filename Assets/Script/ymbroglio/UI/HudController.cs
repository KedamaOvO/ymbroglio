﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HudStatus
{
    Idle,
    Run,
    NoEnergy
}

public class HudController : MonoBehaviour {

    public static HudController Instance;

    public GameObject PlayerHud;
    public GameObject DroneHud;
    public GameObject DangerHud;
    public GameObject NormalTextHud;
    public GameObject StatusReadingHud;
    public Transform EnergyBarHudTransform;

    public DroneControl Drone;
    public PlayerControl Player;

    private Material PlayerHudMaterial;
    private Material DroneHudMaterial;

    private Vector3 lastPlayerPosition;
    private Vector3 lastDronePosition;

    public Color RunColor;
    public Color IdleColor;
    public Color DangerColor;

    private HudStatus PlayerStatus=HudStatus.Idle;
    private HudStatus DroneStatus = HudStatus.Idle;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        PlayerHudMaterial = PlayerHud.GetComponent<Renderer>().material;
        DroneHudMaterial = DroneHud.GetComponent<Renderer>().material;

        //print(IdleColor);

        PlayerHudMaterial.color = IdleColor;
        DroneHudMaterial.color = IdleColor;

        SetDanger(false);
    }

    private void Update()
    {
        //能量槽
        for(int i=0;i<EnergyBarHudTransform.childCount;i++)
        {
            int block_count = (int)(Drone.CurrentEnergy / 10);
            GameObject @object = EnergyBarHudTransform.GetChild(i).gameObject;
            if (i < block_count)
                @object.SetActive(true);
            else
                @object.SetActive(false);
        }

        //Player状态判断
        Vector3 position = Player.transform.position;
        if (position != lastPlayerPosition)
            PlayerStatus = HudStatus.Run;
        else
            PlayerStatus = HudStatus.Idle;
        lastPlayerPosition = position;

        //Drone状态判断
        position = Drone.transform.position;
        if (Drone.CurrentEnergy <= 10e-6)
            DroneStatus = HudStatus.NoEnergy;
        else if (position != lastDronePosition)
            DroneStatus = HudStatus.Run;
        else
            DroneStatus = HudStatus.Idle;
        lastDronePosition = position;

        if (DroneStatus == HudStatus.NoEnergy)
            SetDanger(true);
        else
            SetDanger(false);

        switch(PlayerStatus)
        {
            case HudStatus.Idle:
                PlayerHudMaterial.color = IdleColor;
                break;

            case HudStatus.Run:
                PlayerHudMaterial.color = RunColor;
                break;
        }

        switch (DroneStatus)
        {
            case HudStatus.Idle:
                DroneHudMaterial.color = IdleColor;
                break;

            case HudStatus.Run:
                DroneHudMaterial.color = RunColor;
                break;

            case HudStatus.NoEnergy:
                DroneHudMaterial.color = DangerColor;
                break;
        }
    }

    private void SetDanger(bool danger)
    {
        NormalTextHud.SetActive(!danger);
        DangerHud.SetActive(danger);
    }
}
