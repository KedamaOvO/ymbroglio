﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class MainMenu : MonoBehaviour {

    const string NAME_DAT_PLAYER = "DataPlayer";

    public RectTransform startGameButton;
    public RectTransform continueButton;
    public RectTransform optionButton;
    public RectTransform creditButton;
    public RectTransform exitButton;

    public GameObject OptionPage;
    public GameObject CreditPage;
    public GameObject LoadingPage;
    public GameObject MainMenuPage;

    private Tweener currentCreditPageTweener;

    public float Delay = 0.1f;
    public float Interval = 0.1f;
    public float Duration = 1f;

    private String SavePath;


    void Start(){
        SavePath = Application.dataPath + "/Datas/" + "DataPlayer" + ".json";
        if (CreditPage!=null)
            currentCreditPageTweener = CreditPage.transform
                .Find("Scroll View/Viewport/Content")
                .GetComponent<RectTransform>()
                .DOMoveY(2000, 30);
    }

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			foreach (Transform child in gameObject.transform.Find("other_panel").transform) {
				child.gameObject.SetActive (false);
			}
		}
	}

    private IEnumerator DelayLoadScene(string sceneName,float delay)
    {
        var os = SceneManager.LoadSceneAsync(sceneName);
        os.allowSceneActivation = false;
        yield return new WaitForSeconds(delay);
        os.allowSceneActivation = true;
    }

	public void StartScene(string scenename){
        bool isExist = File.Exists(SavePath);
        Debug.Log(isExist);
        File.Delete(SavePath);
        isExist = File.Exists(SavePath);
        Debug.Log(SavePath);
        Debug.Log(isExist);

        StartCoroutine(DelayLoadScene(scenename, 3f));

        startGameButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay)
            .OnComplete(() => 
            {
                LoadingPage.SetActive(true);
                MainMenuPage.SetActive(false);
            })
            .Play();
        continueButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay + Interval)
            .Play();

        creditButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay+Interval*2)
            .Play();

        optionButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay + Interval*3)
            .Play();

        exitButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay + Interval*4)
            .Play();
    }

    public void OptionClicked()
    {
        /*Component[] comps = option_page.GetComponentsInChildren<Component>();
        Array.ForEach(comps, (c) =>
         {
             if(c is Graphic)
             {
                 var g = c as Graphic;
                 g.DOFade(1, 1).Play();
             }
         });*/
        OptionPage.SetActive(true);
        CreditPage.SetActive(false);
    }

    public void CreditClicked()
    {
        if (!CreditPage.activeInHierarchy)
            currentCreditPageTweener.Restart();

        CreditPage.SetActive(true);
        OptionPage.SetActive(false);
    }

	public void ReStart(){
        FindObjectOfType<ResetSystem>().ResetPlayer();
	}

    public void BacktoMenu() {
        SceneManager.LoadScene("StartMenu2");
    }

    public void ExitGame()
    {
        startGameButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay+Interval*3)
            .Play();

        creditButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay + Interval*2)
            .Play();

        optionButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay + Interval * 1)
            .Play();

        exitButton.DOMoveX(-Screen.width - 500, Duration)
            .SetDelay(Delay)
            .OnComplete(() => Application.Quit())
            .Play();
    }

    public void ContinueClick()
    {
        DataPlayer dataPlayer = DataSystem.Instance.GetDataPlayer();
        if (dataPlayer.sceneName != "")
        {
            DataSystem.Instance.LoadNewScene(dataPlayer.sceneName);
        }
    }
}
