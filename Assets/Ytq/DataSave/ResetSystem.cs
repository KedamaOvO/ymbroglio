﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Interfaces :  ResetSystem.Instance.OnDeath()    
public class ResetSystem : MonoBehaviour 
{
	static ResetSystem instance;

	public delegate void DeathEvent();

	public DeathEvent OnDeath;

	[HideInInspector]
	public GameObject player;
	[HideInInspector]
	public GameObject drone;

	[Header("Drone")]
	public Vector3 droneOffset;

    public AudioClip clip;

	public static ResetSystem Instance
	{
		get{
			return instance;
		}
	}

	[Header("Resetable")]
	public Transform[] resetObjects;

	//To Do Controll checkPoints
	//public CheckPoint[] checkPoints;

	Vector3[] startPositions;

	void Awake()
	{
		instance = this;

		player = GameObject.FindGameObjectWithTag ("Player");
		drone = GameObject.FindGameObjectWithTag ("Drone");
	}
			
	void Start()
	{
		startPositions = new Vector3[resetObjects.Length];

		for (int i = 0; i < startPositions.Length; i++)
		{
			startPositions[i] = resetObjects [i].position;
		}

		//OnDeath += ResetPosition;
		OnDeath += ResetPlayer;

		DataPlayer dataPlayer = DataSystem.Instance.GetDataPlayer ();
		if (dataPlayer.sceneName == SceneManager.GetActiveScene().name)
		{
			LoadPlayerData (dataPlayer);
		}
		
	}
		
	public void ResetPosition()
	{
		for (int i = 0; i < resetObjects.Length; i++)
		{
			resetObjects [i].position = startPositions [i];

			//To Do swtich close etc
			switch (resetObjects [i].tag)
			{
                case "Switch":
                    ButtonExControl controller = resetObjects[i].GetComponent<ButtonExControl>();
                    controller.IsOn = false;
                break;

                case "Inactive":
                    resetObjects[i].gameObject.SetActive(false);
                break;

                case "Active":
                    resetObjects[i].gameObject.SetActive(true);
                    ButtonExControl controller2 = resetObjects[i].GetComponent<ButtonExControl>();
                    if (controller2 != null)
                    {
                        controller2.IsOn = false;
                    }
                break;

                case "MainCamera":
                    resetObjects[i].gameObject.GetComponent<AudioSource>().clip = clip;
                    resetObjects[i].gameObject.GetComponent<AudioSource>().Play();
                break;

                case "Player":
                case "Drone":
                    var living = resetObjects[i].GetComponent<LivingEntity>();
                    living.currentHealth = living.maxHealth;
                    living.dead = false;

                break;
            }
		}
	}
		
	public void LoadPlayerData(DataPlayer dataPlayer)
	{
        StartCoroutine(LoadPlayerDataProcess(dataPlayer));
    }


	public void ResetPlayer()
	{
		
		DataPlayer dataPlayer = DataSystem.Instance.GetDataPlayer ();

		if (dataPlayer.sceneName == DataSystem.Instance.GetCurSceneName ())
		{
			LoadPlayerData (dataPlayer);
			ResetPosition ();
		}
		else
		{
			//To Do Change Scene
		}
	}

    IEnumerator LoadPlayerDataProcess(DataPlayer dataPlayer)
    {
        yield return new WaitForEndOfFrame();

        float pos = 0;
        switch (dataPlayer.curView)
        {
            case "X":
            case "NX":
                pos = dataPlayer.posZ;
                break;
            case "Z":
            case "NZ":
                pos = dataPlayer.posX;
                break;
        }
        GameManager.Instance.SwitchViewMode(dataPlayer.curView, pos);

        player.transform.position = new Vector3(dataPlayer.posX, dataPlayer.posY, dataPlayer.posZ);
        drone.transform.position = player.transform.position + droneOffset;

        //player.transform.rotation = Quaternion.Euler (dataPlayer.rotX, dataPlayer.rotY, dataPlayer.rotZ);
        //drone.transform.rotation = player.transform.rotation;
        GameManager.Instance.CurrentViewMode = dataPlayer.curView;
    }
}
