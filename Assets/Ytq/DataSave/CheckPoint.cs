﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
	//public bool autoSave = true;

	public string collisionTag = "Player";


	void OnTriggerEnter (Collider other)
	{
		
		if (other.tag == collisionTag)
		{
			DataSystem.Instance.SaveDataPlayer (other.transform.position, other.transform.eulerAngles);

				
		}
			
	}
		
}
