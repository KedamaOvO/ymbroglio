﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPro : MonoBehaviour 
{
	public void ContinueClick()
	{
		DataPlayer dataPlayer = DataSystem.Instance.GetDataPlayer ();
		if (dataPlayer.sceneName != "")
		{
			DataSystem.Instance.LoadNewScene (dataPlayer.sceneName);
		}
	}

}
