﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using JsonFx.Json;
using UnityEngine.SceneManagement;

public class DataSystem 
{
	static DataSystem instance;
	 
	string savePath = Application.dataPath + "/Datas/";
    const string NAME_DAT_PLAYER = "DataPlayer";
	const string NAME_EXTENSION = ".json";

	DataPlayer dataPlayer;

	public static DataSystem Instance
	{
		get{ 
			if (instance == null)
			{
				instance = new DataSystem ();
				instance.InitSystem ();
			}
			return instance;
		}
	}

	void InitSystem()
	{
		if (File.Exists (savePath) == false)
		{
			Directory.CreateDirectory (savePath);
		}
		LoadFromLocal ();
	}

	public string GetFilePath(string fileName)
	{
		return savePath + fileName + NAME_EXTENSION;
	}

	public bool ExistFile(string fileName)
	{
		return File.Exists(GetFilePath(fileName));
	}

	public string LoadDataText(string fileName)
	{
		return File.ReadAllText(GetFilePath(fileName));
	}

	public void SaveDataText(string fileName,string dataText)
	{
		File.WriteAllText (GetFilePath (fileName),dataText);
	}
		
	public void LoadFromLocal()
	{
		LoadJsonFile (NAME_DAT_PLAYER, ref dataPlayer);
		if (dataPlayer == null)
		{
			dataPlayer = new DataPlayer ();
		}
	}
		
	bool LoadJsonFile<T>(string fileName,ref T data)
	{
		if (ExistFile (fileName))
		{
			string dataText = LoadDataText (fileName);
			data = JsonReader.Deserialize<T> (dataText);
			return true;
		}
		else
		{
			return false;
		}
	}

	public void SaveJsonFile<T>(string fileName,T data)
	{
		string dataText = JsonWriter.Serialize (data);
		SaveDataText (fileName,dataText);
	}

	public void SaveDataPlayer(Vector3 position,Vector3 rotation)
	{
        Debug.Log("Save");

		dataPlayer.posX = position.x;
		dataPlayer.posY = position.y;
		dataPlayer.posZ = position.z;

		/*
		dataPlayer.rotX = rotation.x;
		dataPlayer.rotY = rotation.y;
		dataPlayer.rotZ = rotation.z;
		*/

		dataPlayer.curView = GameManager.Instance.CurrentViewMode;

		dataPlayer.sceneName = GetCurSceneName();
		SaveJsonFile (NAME_DAT_PLAYER, dataPlayer);
	}
		
	public string GetCurSceneName()
	{
		return SceneManager.GetActiveScene ().name;
	}

	public DataPlayer GetDataPlayer()
	{
		return dataPlayer;
	}

	public void LoadNewScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}
}
