﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour 
{

	public bool canMove = false;
	public AnimationCurve moveCurve;

	public float moveDistanceX;
	public float moveTime;

	public bool reverseXY = false;

	float percent = 0;
	float speed =0;
	[HideInInspector]
	public Vector3 offset;

	[HideInInspector]
	public Vector3 startPostion;
	float moveSpeed;
	[HideInInspector]
	public float sign;

	public float attack;

	[Header("HitEffect")]
	public GameObject hitEffect;
	public float effectTime;
	//public Sound hitSound;

	void Awake()
	{
		speed = 1/ moveTime;
		offset.x = moveDistanceX;
		offset.y = moveCurve.Evaluate (1) * moveDistanceX;
		offset.z = 0;

		moveSpeed = moveDistanceX / moveTime;
	}

	void FixedUpdate()
	{
		if (canMove)
		{
			if (percent < 1)
			{
				Vector3 evaluateOffset;
				if (!reverseXY)
				{
					evaluateOffset = new Vector3 (sign * moveDistanceX * percent, moveCurve.Evaluate (percent) * moveDistanceX, 0);
				} else
				{
					evaluateOffset = new Vector3 (moveCurve.Evaluate (percent) * moveDistanceX,sign * moveDistanceX * percent,0);
				}
				transform.localPosition = (startPostion + evaluateOffset);
				percent += Time.deltaTime * speed;
				//print (percent);
			} 
			else
			{
				Destroy (gameObject);
			}
		}
	}

	bool hasAppliedDamage = false;
	public string collisionTag = "Enemy";

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == collisionTag)
		{
			if (!hasAppliedDamage)
			{
				hasAppliedDamage = true;
				LivingEntity targetEntity = other.GetComponent<LivingEntity> ();

				if (hitEffect != null)
				{
					ShowAttackEffect (other.transform);
					//StartCoroutine (ShowAttackEffect (other.transform));
				}

				/*
				if (!string.IsNullOrEmpty (hitSound.clipName))
				{
					AudioSystem.instance.PlaySound (hitSound);
				}
				*/

				targetEntity.AcceptDamage (attack);
				Destroy (gameObject);
			}
		} 

	}

	void OnCollisionEnter(Collision other)
	{
		if (other.transform.tag == collisionTag)
		{
			if (!hasAppliedDamage)
			{
				hasAppliedDamage = true;
				LivingEntity targetEntity = other.transform.GetComponent<LivingEntity> ();

				if (hitEffect != null)
				{
					ShowAttackEffect (other.transform);
					//StartCoroutine (ShowAttackEffect (other.transform));
				}

				/*
				if (!string.IsNullOrEmpty (hitSound.clipName))
				{
					AudioSystem.instance.PlaySound (hitSound);
				}
				*/

				targetEntity.AcceptDamage (attack);
				Destroy (gameObject);
			}
		} 

	}

	void ShowAttackEffect(Transform other)
	{
		GameObject effectEntity = Instantiate (hitEffect);
		effectEntity.transform.position = other.position;

	}
}
