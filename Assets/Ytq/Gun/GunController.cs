﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour 
{


	[Header("Projectile")]
	public Transform[] muzzleArray;
	public GameObject bullet;

	[Header("Attack")]
	public bool canAttack;
	//[HideInInspector]
	float nextAttackTime; 
	public float timeBetweenAttack;
	public KeyCode attackKey;
	public float animationTime;

	public bool isEnemy = false;


	void FixedUpdate()
	{
		if (canAttack) 
		{

			if (Time.time > nextAttackTime) 
			{
				if (!isEnemy)
				{
					if (Input.GetKey (attackKey))
					{
						DoProjection ();
						//OnAttackInput ();
						nextAttackTime = Time.time + timeBetweenAttack;
					}
				} else
				{

					DoProjection ();
					//OnAttackInput ();
					nextAttackTime = Time.time + timeBetweenAttack;

				}
			}

		}
	}

	public void DoProjection()
	{
		for (int i = 0; i < muzzleArray.Length; i++) 
		{
			GameObject newBullet = Instantiate (bullet);

			newBullet.transform.position = muzzleArray [i].position;

			BulletController controller = newBullet.GetComponent<BulletController> ();
			controller.startPostion = newBullet.transform.localPosition;
			controller.sign = muzzleArray [i].eulerAngles.y > 90 ? -1 : 1;
			controller.canMove = true;
		}
	}

	public void OnAttackInput()
	{
		//To Do Player Animation
	}


}
