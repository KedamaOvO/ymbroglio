﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LivingEntity : MonoBehaviour,IDamagable
{
    public float maxHealth;
    public float currentHealth;
    public bool dead;
    public float attack;

    public event Action OnDamage;
    public event Action<Transform> OnDeath;

    protected virtual void Start()
    {
        currentHealth = maxHealth;
        OnDeath += t => GameManager.Instance.GameOver();
    }

    #region IDamager implementation
    public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        //To Do Play Death Music and Show Death Effect

        AcceptDamage(damage);
    }

    public virtual void AcceptDamage(float damage)
	{
		currentHealth -= damage;
		
		if (OnDamage != null)
		{
			OnDamage ();
		}

		if (currentHealth <= 0 && !dead)
		{
			Die ();
		}

	}
    #endregion

    public virtual void Die()
    {
        dead = true;
        if (OnDeath != null)
        {
            OnDeath(transform);
        }
       // OnDeath = null;
       // GameObject.Destroy(gameObject);
    }

	
}
