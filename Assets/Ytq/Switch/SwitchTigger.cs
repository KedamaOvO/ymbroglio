﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchTigger : MonoBehaviour {

	public string collisionTag = "Player";

	[Header("Switch")]
	public bool isOn = false;
	public GameObject rightTip;
	public GameObject wrongTip;

	[Header("Input")]
	public bool needInput = true;
	public KeyCode toggleKey = KeyCode.J;

	void OnTriggerStay(Collider other)
	{
		if (isOn)
		{
			return;
		}

		if (other.tag == collisionTag)
		{
			if (needInput)
			{
				if (Input.GetKey (toggleKey))
				{
					SwitchSystem.Instance.OnSwitchOpen (this);
					isOn = true;
				}
			}
			else
			{
				SwitchSystem.Instance.OnSwitchOpen (this);
				isOn = true;
			}
		}
	}

	public void ShowTip(bool isRight)
	{
		rightTip.SetActive (isRight);
		wrongTip.SetActive (!isRight);
	}
}
