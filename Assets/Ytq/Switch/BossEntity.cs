﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossEntity : LivingEntity
{
	public Slider lifeSlider;

	void Awake()
	{
		//OnDamage += SetLifeSlider;
	}

	void Update()
	{
		lifeSlider.value = currentHealth / maxHealth;
	}

}
