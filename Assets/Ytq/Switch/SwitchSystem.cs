﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSystem : MonoBehaviour 
{
	public enum SwitchOrder
	{
		A = 0,B,C,D
	};

	[System.Serializable]
	public struct SwitchUnit
	{
		public SwitchTigger trigger;
		public SwitchOrder order;

	}

	[Header("Switch")]
	public SwitchUnit[] switchArray;
	public int[] orderArray;
	static SwitchSystem instance;

	public static SwitchSystem Instance
	{
		get{
			return instance;
		}
	}

	[Header("RightSwitchCount")]
	public int counter = 0;

	[Header("HideTip")]
	public float resetTime=1f;

	[Header("Boss")]
	public LivingEntity bossEntity;
		
	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		orderArray = new int[switchArray.Length];
		for (int i = 0; i < switchArray.Length; i++)
		{
			orderArray [(int)switchArray [i].order] = i;
		}
	}

	public void OnSwitchOpen(SwitchTigger trigger)
	{
		if (trigger == switchArray [counter].trigger)
		{
			trigger.ShowTip (true);
			counter++;
			CounterCheck ();
		}
		else
		{
			trigger.ShowTip (false);
			for (int i = 0; i < counter; i++)
			{
				switchArray [i].trigger.ShowTip (false);
			}
			counter = 0;
			StartCoroutine (ResetCoroutine ());
		}
	}



	public void DamageBoss()
	{
		bossEntity.AcceptDamage (1.0F/switchArray.Length * bossEntity.maxHealth);

	}

	public void CounterCheck()
	{
		if (counter <= orderArray.Length)
		{
			DamageBoss ();
		}

		if (counter == orderArray.Length)
		{
			
			//To Do Win Game
		}
	}

	IEnumerator ResetCoroutine()
	{
		yield return new WaitForSeconds(resetTime);

		foreach (SwitchUnit unit in switchArray)
		{
			unit.trigger.isOn = false;
            unit.trigger.wrongTip.SetActive (false);
		}

		bossEntity.currentHealth = bossEntity.maxHealth;
	}


}
